const express = require('express')
const app = express()

const cors = require('cors')
const PORT  = process.env.PORT || 5000

app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use(cors())


app.use('/', require('./routes/foobarqix'))

app.listen(PORT, ()=> console.log(`App listening on port ${PORT}`))