import React, { useState } from 'react';
import './App.css';

import FooBarQix from './components/FooBarQix'

const App = () => {
  let loading;
  const [fooBarQix, setFooBarQix] = useState({ startIndex: 0, endIndex: 0, data: [] })


  const sendForm = async (event) => {
    event.preventDefault()
    loading = true
    const target = event.target
    let startIndex = target[0].value
    let endIndex = target[1].value
    try {
      const response = await fetch('http://localhost:5000', {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ startIndex, endIndex })
      })
      const data = await response.json()
      console.log(data)
      setFooBarQix(data)
    } catch (error) {
      console.log(error)
    }
    loading = false
  }
  return (
    <div className="App">
      <div className="limit-input-ctn">
        <div className="limit-input-box"></div>
        <h1>Choose your FooBarQix range</h1>
        <p>(server-side computing)</p>
        <form onSubmit={event => sendForm(event)}>
          <input type="text" id="start-index" />
          <input type="text" id="end-index" />
          <input type="submit" value="compute" />
        </form>
      </div>

      <FooBarQix fooBarQix={fooBarQix} loading={loading} />
    </div >
  );
}

export default App;
