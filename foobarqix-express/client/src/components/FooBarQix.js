import React from "react";


const FooBarQix = ({ fooBarQix, loading }) => {

    const { startIndex, data } = fooBarQix
    console.log(fooBarQix)

    let display

    if (loading === true) {
        display = <p>loading</p>
    }
    else {
        display = data.map((number, index) => {
            return (
                <div className="foobarqix-box" key={index}>
                    <p className="foobarqix-input"> {parseInt(startIndex) + index} </p>
                    <p className="foobarqix-change">  =>  </p>
                    <p className="" >{number} </p>
                </div>
            )
        })
    }
    // DISPLAY LOGIC



    // DISPLAY
    return (
        <div className="foobarqix-ctn">
            {display}
        </div>
    )
};

export default FooBarQix;




