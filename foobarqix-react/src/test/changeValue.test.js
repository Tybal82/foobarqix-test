import { changeValue } from './changeValue'


describe('Checking if the function changeValue(string) outputs the right value ', () => {
    test('when input is "1", it outputs "1"', () => {
        expect(changeValue('1')).toBe('1')
    })
    test('when input is "2", it outputs "2"', () => {
        expect(changeValue('2')).toBe('2')
    })
    test('when input is "3", it outputs "FooFoo"', () => {
        expect(changeValue('3')).toBe('FooFoo')
    })
    test('when input is "4", it outputs "4"', () => {
        expect(changeValue('4')).toBe('4')
    })
    test('when input is "5", it outputs "BarBar"', () => {
        expect(changeValue('5')).toBe('BarBar')
    })
    test('when input is "6", it outputs "Foo"', () => {
        expect(changeValue('6')).toBe('Foo')
    })
    test('when input is "7", it outputs "QixQix"', () => {
        expect(changeValue('7')).toBe('QixQix')
    })
    test('when input is "8", it outputs "8"', () => {
        expect(changeValue('8')).toBe('8')
    })
    test('when input is "9", it outputs "Foo"', () => {
        expect(changeValue('9')).toBe('Foo')
    })
    test('when input is "10", it outputs "Bar*"', () => {
        expect(changeValue('10')).toBe('Bar*')
    })
    test('when input is "13", it outputs "Foo"', () => {
        expect(changeValue('13')).toBe('Foo')
    })
    test('when input is "15", it outputs "FooBarBar"', () => {
        expect(changeValue('15')).toBe('FooBarBar')
    })
    test('when input is "21", it outputs "FooQix"', () => {
        expect(changeValue('21')).toBe('FooQix')
    })
    test('when input is "33", it outputs "FooFooFoo"', () => {
        expect(changeValue('33')).toBe('FooFooFoo')
    })
    test('when input is "51", it outputs "FooBar"', () => {
        expect(changeValue('51')).toBe('FooBar')
    })
    test('when input is "53", it outputs "BarFoo"', () => {
        expect(changeValue('53')).toBe('BarFoo')
    })
    test('when input is "101", it outputs "1*1"', () => {
        expect(changeValue('101')).toBe('1*1')
    })
    test('when input is "303", it outputs "FooFoo*Foo"', () => {
        expect(changeValue('303')).toBe('FooFoo*Foo')
    })
    test('when input is "105", it outputs "FooBarQix*Bar"', () => {
        expect(changeValue('105')).toBe('FooBarQix*Bar')
    })
    test('when input is "101O1", it outputs "FooQix**"', () => {
        expect(changeValue('10101')).toBe('FooQix**')
    })
})






