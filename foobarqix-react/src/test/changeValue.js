
let foo = 'Foo';
let bar = 'Bar';
let qix = 'Qix';
let output = '';
let char = '';
let newOutput = '';
let step = 0;


export const changeValue = (string) => {
    // clear output
    output = "";
    // check if the input is divisible by 3 or 5 or 7 
    // if so, concatenate the right value to the output
    if (parseInt(string) % 3 === 0) output += foo;
    if (parseInt(string) % 5 === 0) output += bar;
    if (parseInt(string) % 7 === 0) output += qix;

    // parse the input and check if it contains 3, 5, 7 or 0
    // if so, concatenate the right value to the output
    for (step = 0; step < string.length; step++) {
        char = string.charAt(step);
        if (char === '3') output += foo;
        if (char === '5') output += bar;
        if (char === '7') output += qix;

        if (char === '0') {
            let split = string.split('');
            let findIndex = split.indexOf('0');
            split.splice(findIndex, 1, '*');
            let join = split.join('');
            string = join
        }
        // if the output contains letters,
        // clears any stringified number but '0' in it
        // and then replace any '0' by '*'
        if (output.match(/[a-zA-Z]/)) {
            newOutput = char.replace(/0/g, '*').replace(/[1-9]/g, '');
            output += newOutput;
        }
    }

    if (output === "") return string
    else return output
}