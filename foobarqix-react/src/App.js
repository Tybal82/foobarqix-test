import React, { useState } from 'react';
import './App.css';

import FooBarQix from './components/FooBarQix'

const App = () => {
    const [limit, setLimit] = useState(150)
    return (
        <section className="App" >
            <div className="limit-input-ctn">
                <div className="limit-input-box">
                    <h1>Choose your FooBarQix limit</h1>
                    <p>(client-side computing)</p>
                    <input type="text" className="limit-input"
                        value={limit}
                        onChange={event => setLimit(event.target.value)}
                    />
                </div>
            </div>
            <FooBarQix limit={limit} />
        </section>
    );
}

export default App;